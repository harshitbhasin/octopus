target_sources(Octopus_lib PRIVATE
		atom.F90
		born_charges.F90
		ion_dynamics.F90
		ions.F90
		read_coords.F90
		vibrations.F90
		symmetries.F90
		symmetries_finite.c
		)

## External libraries
target_link_libraries(Octopus_lib PRIVATE Spglib::fortran)
