Eigenvalues [eV]
 #st  Spin   Eigenvalue      Occupation
   1   --   -15.990926       2.000000
   2   --    -9.065616       2.000000
   3   --    -9.065616       2.000000
   4   --    -9.065616       2.000000

Energy [eV]:
      Total       =      -219.03751043
      Free        =      -219.03751043
      -----------
      Ion-ion     =       236.08498119
      Eigenvalues =       -86.37554993
      Hartree     =       393.45819611
      Int[n*v_xc] =      -105.38067937
      Exchange    =       -69.66885295
      Correlation =       -11.00057200
      vanderWaals =         0.00000000
      Delta XC    =         0.00000000
      Entropy     =         0.00000000
      -TS         =        -0.00000000
      Photon ex.  =         0.00000000
      Kinetic     =       163.97293343
      External    =      -931.88444364
      Non-local   =       -49.75841882
      Int[n*v_E]  =         0.00000000

