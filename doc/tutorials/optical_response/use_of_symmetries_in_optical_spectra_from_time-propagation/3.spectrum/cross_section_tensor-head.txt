# nspin         1
# kick mode    0
# kick strength    0.005291772086
# dim           3
# pol(1)           1.000000000000    0.000000000000    0.000000000000
# pol(2)           0.000000000000    1.000000000000    0.000000000000
# pol(3)           0.000000000000    0.000000000000    1.000000000000
# direction    1
# Equiv. axes  3
# wprime           0.000000000000    0.000000000000    1.000000000000
# kick time        0.000000000000
#       Energy         (1/3)*Tr[sigma]    Anisotropy[sigma]      sigma(1,1,1)        sigma(1,2,1)        sigma(1,3,1)        sigma(2,1,1)        sigma(2,2,1)        sigma(2,3,1)        sigma(3,1,1)        sigma(3,2,1)        sigma(3,3,1)    
#        [eV]               [A^2]               [A^2]               [A^2]               [A^2]               [A^2]               [A^2]               [A^2]               [A^2]               [A^2]               [A^2]               [A^2]        
      0.00000000E+00      0.00000000E+00      0.00000000E+00      0.00000000E+00      0.00000000E+00      0.00000000E+00      0.00000000E+00      0.00000000E+00      0.00000000E+00      0.00000000E+00      0.00000000E+00      0.00000000E+00
      0.10000000E-01     -0.26424880E-09      0.24825139E-15     -0.26424880E-09     -0.54193533E-16     -0.11804598E-15     -0.54193533E-16     -0.26424880E-09     -0.11804598E-15     -0.11804598E-15     -0.11804598E-15     -0.26424880E-09
      0.20000000E-01     -0.10451862E-08      0.99239626E-15     -0.10451862E-08     -0.21651704E-15     -0.47162599E-15     -0.21651704E-15     -0.10451862E-08     -0.47162599E-15     -0.47162599E-15     -0.47162599E-15     -0.10451862E-08
      0.30000000E-01     -0.23073310E-08      0.22265555E-14     -0.23073310E-08     -0.48620073E-15     -0.10590695E-14     -0.48620073E-15     -0.23073310E-08     -0.10590695E-14     -0.10590695E-14     -0.10590695E-14     -0.23073310E-08
      0.40000000E-01     -0.39913689E-08      0.39469207E-14     -0.39913689E-08     -0.86196658E-15     -0.18776030E-14     -0.86196658E-15     -0.39913689E-08     -0.18776030E-14     -0.18776030E-14     -0.18776030E-14     -0.39913689E-08
      0.50000000E-01     -0.60138943E-08      0.61512699E-14     -0.60138943E-08     -0.13420357E-14     -0.29233663E-14     -0.13420357E-14     -0.60138943E-08     -0.29233663E-14     -0.29233663E-14     -0.29233663E-14     -0.60138943E-08
      0.60000000E-01     -0.82670660E-08      0.88100538E-14     -0.82670660E-08     -0.19241385E-14     -0.41914348E-14     -0.19241385E-14     -0.82670660E-08     -0.41914348E-14     -0.41914348E-14     -0.41914348E-14     -0.82670660E-08
