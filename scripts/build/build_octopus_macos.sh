#!/bin/bash

# Emulate bash to be able to execute properly the pushd and popd commands
emulate bash

# Set language (which is a bit tricky for MacOS)
export LANG="en_US"
export LC_CTYPE="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"

# Determine the number of available processors. It is better to use only physical cpus
n_proc=$(sysctl -n hw.physicalcpu)
# Determine the installation path
pushd .. && path=$(pwd) && popd

# Compiler flags (Optimization and debugging info)
export CFLAGS="-O3 -g"
export FCFLAGS="$CFLAGS -fopenmp"
export CXXFLAGS="$CFLAGS -std=c++14"
# We have to override the CPlusPlus libs because otherwise it conflics with an non-linkable XCode lib.
export CXXLIBS="-lto_library -L/opt/homebrew -L/opt/homebrew/bin -L/opt/homebrew/opt/lapack/lib -L/opt/homebrew/Cellar/open-mpi/4.1.4_2/lib -L/opt/homebrew/Cellar/gcc/12.2.0/lib -L/usr/local/lib -lmpi -lc++ -lSystem"

# Skip automake step
if [[ "$1" != "noconfig" ]]; then
  pushd ../octopus && autoreconf -i && popd
fi

../octopus/configure \
   --prefix="$path/installed" \
   CC="/opt/homebrew/bin/mpicc" \
   FC="/opt/homebrew/bin/mpif90" \
   CXX="/opt/homebrew/bin/mpicxx" \
   --enable-mpi \
   --enable-openmp \
   --with-libxc-prefix="/opt/homebrew" \
   --with-gsl-prefix="/opt/homebrew" \
   --with-fftw-prefix="/opt/homebrew" \
   --with-blas="/opt/homebrew/Cellar/lapack/3.10.1_1/lib/libblas.dylib" \
   --with-lapack="/opt/homebrew/Cellar/lapack/3.10.1_1/lib/liblapack.dylib" \
   --with-blacs="/opt/homebrew/Cellar/scalapack/2.2.0_1/lib/libscalapack.dylib" \
   --with-scalapack="/opt/homebrew/Cellar/scalapack/2.2.0_1/lib/libscalapack.dylib" \

make -j $n_proc && make -j $n_proc install

emulate -R zsh
