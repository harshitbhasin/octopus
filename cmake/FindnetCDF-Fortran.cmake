#[==============================================================================================[
#                                 netCDF compatibility wrapper                                 #
]==============================================================================================]

#[===[.md
# FindnetCDF-Fortran

netCDF compatibility module for Octopus

This file is specifically tuned for Octopus usage. See `Octopus_FindPackage` for a more general
interface.

]===]

include(Octopus)
Octopus_FindPackage(${CMAKE_FIND_PACKAGE_NAME}
		NAMES netCDF-Fortran
		PKG_MODULE_NAMES netcdf-fortran)

# Create appropriate aliases
if (${CMAKE_FIND_PACKAGE_NAME}_PKGCONFIG)
	add_library(netCDF::Fortran ALIAS PkgConfig::${CMAKE_FIND_PACKAGE_NAME})
elseif (NOT TARGET netCDF::Fortran)
	# Upstream has a horrible naming scheme :(
	# https://github.com/Unidata/netcdf-fortran/issues/404
	if (TARGET netCDF::netcdff)
		add_library(netCDF::Fortran ALIAS netCDF::netcdff)
	else ()
		add_library(netCDF::Fortran ALIAS netcdff)
	endif ()
endif ()
