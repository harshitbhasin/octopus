# The output of fields is currently broken (to be fixed in 13.1).
# Until then the plot of the fields is disabled.

# set term png size 1000,400
set term png size 600,400
set output 'tutorial_01_run_maxwell_energy_and_fields.png'

# set multiplot
set style data l

set xlabel 'time [a.u.]'

# set origin 0.025,0
# set size 0.45,0.9
# set title 'Electric and magnetic fields'
# set ylabel 'Maxwell fields [a.u.]'
# p 'Maxwell/td.general/total_e_field_z' u 2:3 t 'E_z', 'Maxwell/td.general/total_b_field_y' u 2:($3*100) t '100 * B_y'


# set origin 0.525,0
# set size 0.45,0.9
set title 'Maxwell energy'
set ylabel 'Maxwell energy [a.u.]'
p  'Maxwell/td.general/maxwell_energy' u 2:3  t 'Maxwell energy'

# unset multiplot
