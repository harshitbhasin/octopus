target_sources(Octopus_lib PRIVATE
		box.F90
		box_cgal.F90
		box_cylinder.F90
		box_factory.F90
		box_image.F90
		box_minimum.F90
		box_parallelepiped.F90
		box_shape.F90
		box_sphere.F90
		box_union.F90
		box_user_defined.F90
		multibox.F90
		)
## Unused sources
# box_intersection.F90
