# -*- coding: utf-8 mode: shell-script -*-

Test       : 1D-Lithium
Program    : octopus
TestGroups : short-run; finite_systems_1d
Enabled    : Yes

Input: 04-lithium.01-ground_state.inp

# Energies
Precision: 8.08e-07
match ; Total energy    ; GREPFIELD(static/info, 'Total       =', 3) ; -4.178845955
Precision: 8.70e-06
match ; Eigenvalues     ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -1.7390770
Precision: 8.36e-07
match ; Hartree         ; GREPFIELD(static/info, 'Hartree     =', 3) ; 2.7065381
Precision: 1.00e-06
match ; Int[n*v_xc]     ; GREPFIELD(static/info, 'Int\[n\*v_xc\] =', 3) ; -1.30972551
Precision: 1.00e-06
match ; Exchange        ; GREPFIELD(static/info, 'Exchange    =', 3) ; -1.00237718
Precision: 1.00e-06
match ; Correlation     ; GREPFIELD(static/info, 'Correlation =', 3) ; -0.04057932
Precision: 1.00e-06
match ; Kinetic         ; GREPFIELD(static/info, 'Kinetic     =', 3) ;  0.58693155
Precision: 1.20e-06
match ; External        ; GREPFIELD(static/info, 'External    =', 3) ; -6.42935889

# Dipole
Precision: 5e-06
match ; Dipole          ; GREPFIELD(static/info, '<x> =', 3) ; 0.0

Input: 04-lithium.02-absorbing_boundaries.inp

# Note, one could compare td.general/multipoles column 3 + 5, to td.general/norm_wavefunctions column 2
# but the test framework does not support this operation.

# 6 lines of header
Precision: 2.e-7
match ;   N_electrons     [step  0]     ; LINEFIELD(td.general/norm_wavefunctions,    7, 3) ; 3.00
Precision: 1.82e-07
match ;   N_electrons     [step  500]   ; LINEFIELD(td.general/norm_wavefunctions, 507, 3) ; 2.9261576470677833
Precision: 3.06e-07
match ;   N_electrons     [step  1112]  ; LINEFIELD(td.general/norm_wavefunctions, 1119, 3) ; 2.35301005211766

Precision: 1.30e-07
match ;   norm11     [step  0]     ; LINEFIELD(td.general/norm_wavefunctions,    7, 4) ; 1.00
match ;   norm11     [step  500]   ; LINEFIELD(td.general/norm_wavefunctions,  507, 4) ; 9.8483603893061722e-01
match ;   norm11     [step  1112]  ; LINEFIELD(td.general/norm_wavefunctions, 1119, 4) ; 8.6370998478391403e-01

match ;   norm21     [step  0]     ; LINEFIELD(td.general/norm_wavefunctions,    7, 5) ; 1.00
match ;   norm21     [step  500]   ; LINEFIELD(td.general/norm_wavefunctions,  507, 5) ; 9.9238278883920150e-01
Precision: 3e-07
match ;   norm21     [step  1112]  ; LINEFIELD(td.general/norm_wavefunctions, 1119, 5) ; 9.1995542547488052e-01
