## Internal objects
# Basic objects and interface
add_library(fortran_cli OBJECT)
target_link_libraries(fortran_cli PRIVATE Octopus_base)

# Dependent objects
set(OctopusDependentObjects "")
list(APPEND OctopusDependentObjects
		basic_mpi # Depends on mpi (optional)
		dftbplus_dftb # Depends on dftbplus (optional)
		electrons_v_ks # Depends on dftd3 (bundled)
		ions_symmetries # Depends on spglib (bundled)
		math_blas # Depends on blas (required)
		math_bpdn # Depends on bpdn (bundled)
		math_fft # Depends on FFT libraries (various)
		math_lapack # Depends on lapack libraries (required)
		math_metis # Depends on metis (bundled)
		math_qshep # Depends on qshep (bundled)
		output_berkeleygw # Depends on berkleygw (optional)
		species_rapidxml # Depends on rapidxml (bundled)
		# Breaks naming convention, but this one is all over the place
		xc_oct # Depends on libxc (required)
		)
# Folder based objects
set(OctopusFolderObjects "")
list(APPEND OctopusFolderObjects
		basic
		basis_set
		boxes
		classical
		electrons
		grid
		hamiltonian
		interactions
		ions
		math
		maxwell
		multisystem
		opt_control
		output
		photons
		poisson
		scf
		species
		states
		sternheimer
		td
		)

foreach (subdir IN LISTS OctopusFolderObjects)
	add_subdirectory(${subdir})
endforeach ()
# Leftover directories
add_subdirectory(dftbplus)
add_subdirectory(include)
add_subdirectory(library)
add_subdirectory(main)
add_subdirectory(utils)

## Special treatment

## Unused sources
# boxes/box_intersection.F90
# electrons/pert.F90
# grid/operate_inc.c
# scf/mixing_metric.F90
