# -*- coding: utf-8 mode: shell-script -*-

Test       : XC Functionals 1D
Program    : octopus
TestGroups : short-run, functionals
Enabled    : Yes

Input: 01-xc_1d.01-wfs-lda.inp
match ; SCF convergence                          ; GREPCOUNT(static/info, 'SCF converged') ; 1
Precision: 1.00e-04
match ;  LDA Total Energy                          ; GREPFIELD(static/info, 'Total       =', 3) ; -4.17967691
Precision: 1.00e-04
match ;  LDA Exchange                              ; GREPFIELD(static/info, 'Exchange    =', 3) ; -1.00241236
Precision: 1.00e-04
match ;  LDA Correlation                           ; GREPFIELD(static/info, 'Correlation =', 3) ; -0.04057719
Precision: 1.00e-04
match ;  LDA Int[n*v_xc]                           ; GREPFIELD(static/info, 'Int\[n\*v_xc\] =', 3) ; -1.30968982
Precision: 3.99e-05
match ;  LDA Eigenvalue 1 up                       ; GREPFIELD(static/info, '  1   up', 3) ; -0.797085
Precision: 8.36e-06
match ;  LDA Eigenvalue 2 up                       ; GREPFIELD(static/info, '  2   up', 3) ; -0.167262
Precision: 3.88e-07
match ;  LDA Eigenvalue 1 dn                       ; GREPFIELD(static/info, '  1   dn', 3) ; -0.775148
Precision: 4.37e-05
match ;  LDA Eigenvalue 2 dn                       ; GREPFIELD(static/info, '  2   dn', 3) ; -0.08741900000000001

Input: 01-xc_1d.02-wfs-hf.inp
match ; SCF convergence                          ; GREPCOUNT(static/info, 'SCF converged') ; 1
Precision: 5.63e-06
match ;  Hartree-Fock Eigenvalue 1 up              ; GREPFIELD(static/info, '  1   up', 3) ; -1.1261889999999999
Precision: 1.58e-05
match ;  Hartree-Fock Eigenvalue 2 up              ; GREPFIELD(static/info, '  2   up', 3) ; -0.315508
Precision: 5.36e-06
match ;  Hartree-Fock Eigenvalue 1 dn              ; GREPFIELD(static/info, '  1   dn', 3) ; -1.071001
Precision: 3.01e-17
match ;  Hartree-Fock Eigenvalue 2 dn              ; GREPFIELD(static/info, '  2   dn', 3) ; 0.0030120000000000004
Precision: 1.00e-04
match ;  Hartree-Fock Total Energy                  ; GREPFIELD(static/info, 'Total       =', 3) ; -5.2736276
Precision: 1.00e-04
match ;  Hartree-Fock Exchange Energy              ; GREPFIELD(static/info, 'Exchange    =', 3) ; -1.0771783

Input: 01-xc_1d.lda_csc.inp
Precision: 3.88e-04
match ;  LDA CSC Eigenvalue 1 up                   ; GREPFIELD(static/info, '  1   up', 3) ; -0.77564
Precision: 7.73e-06
match ;  LDA CSC Eigenvalue 2 up                   ; GREPFIELD(static/info, '  2   up', 3) ; -0.154591
Precision: 3.75e-05
match ;  LDA CSC Eigenvalue 1 dn                   ; GREPFIELD(static/info, '  1   dn', 3) ; -0.750023
Precision: 1.47e-05
match ;  LDA CSC Eigenvalue 2 dn                   ; GREPFIELD(static/info, '  2   dn', 3) ; 0.002939
Precision: 1.00e-04
match ;  LDA Exchange                              ; GREPFIELD(static/info, 'Exchange    =', 3) ; -1.019315
Precision: 1.00e-04
match ;  LDA CSC Correlation                       ; GREPFIELD(static/info, 'Correlation =', 3) ; -0.03745068
Precision: 1.00e-04
match ;  LDA CSC Int[n*v_xc]                       ; GREPFIELD(static/info, 'Int\[n\*v_xc\] =', 3) ; -1.3219134

Input: 01-xc_1d.oep_kli.inp
Precision: 4.74e-05
match ;  OEP KLI Eigenvalue 1 up                   ; GREPFIELD(static/info, '  1   up', 3) ; -0.948877
Precision: 1.58e-05
match ;  OEP KLI Eigenvalue 2 up                   ; GREPFIELD(static/info, '  2   up', 3) ; -0.315508
Precision: 5.36e-06
match ;  OEP KLI Eigenvalue 1 dn                   ; GREPFIELD(static/info, '  1   dn', 3) ; -1.071001
Precision: 3.63e-05
match ;  OEP KLI Eigenvalue 2 dn                   ; GREPFIELD(static/info, '  2   dn', 3) ; -0.022254
Precision: 1.10e-03
match ;  OEP KLI Exchange                          ; GREPFIELD(static/info, 'Exchange    =', 3) ; -1.07717828
Precision: 1.10e-03
match ;  OEP KLI Int[n*v_xc]                       ; GREPFIELD(static/info, 'Int\[n\*v_xc\] =', 3) ; -1.97704513

Input: 01-xc_1d.oep_exx.inp
Precision: 4.74e-05
match ;  OEP EXX Eigenvalue 1 up                   ; GREPFIELD(static/info, '  1   up', 3) ; -0.948877
Precision: 1.58e-05
match ;  OEP EXX Eigenvalue 2 up                   ; GREPFIELD(static/info, '  2   up', 3) ; -0.315508
Precision: 5.36e-06
match ;  OEP EXX Eigenvalue 1 dn                   ; GREPFIELD(static/info, '  1   dn', 3) ; -1.071001
Precision: 3.63e-05
match ;  OEP EXX Eigenvalue 2 dn                   ; GREPFIELD(static/info, '  2   dn', 3) ; -0.022254
Precision: 1.10e-03
match ;  OEP Exact Exchange                        ; GREPFIELD(static/info, 'Exchange    =', 3) ; -1.07717828
Precision: 1.10e-03
match ;  OEP EXX Int[n*v_xc]                       ; GREPFIELD(static/info, 'Int\[n\*v_xc\] =', 3) ; -1.97704512
