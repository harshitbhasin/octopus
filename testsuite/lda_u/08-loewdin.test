# -*- coding: utf-8 mode: shell-script -*-

Test       : Loewdin orthogonalization and intersite ACBN0+V functional
Program    : octopus
TestGroups : long-run, periodic_systems, lda_u
Enabled    : Yes

Processors : 3

Input      : 08-loewdin.01-Si.inp

match ;  SCF convergence  ; GREPCOUNT(static/info, 'SCF converged') ; 1.0

Precision: 3.95e-07
match ;   Total energy         ; GREPFIELD(static/info, 'Total       =', 3) ; -7.8974566
Precision: 3.93e-06
match ;   Ion-ion energy       ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -7.8578008
Precision: 1.08e-07
match ;   Eigenvalues sum      ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -0.21515352
Precision: 2.82e-07
match ;   Hartree energy       ; GREPFIELD(static/info, 'Hartree     =', 3) ; 0.56447369
Precision: 1.02e-07
match ;   Exchange energy      ; GREPFIELD(static/info, 'Exchange    =', 3) ; -2.03940392
Precision: 1.88e-07
match ;   Correlation energy   ; GREPFIELD(static/info, 'Correlation =', 3) ; -0.37540186
Precision: 1.55e-07
match ;   Kinetic energy       ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 3.10653371
Precision: 6.51e-08
match ;   External energy      ; GREPFIELD(static/info, 'External    =', 3) ; -1.3017473600000002
Precision: 2.94e-07
match ;   Hubbard energy       ; GREPFIELD(static/info, 'Hubbard     =', 3) ; 0.005889940000000001

Precision: 8.06e-05
match ;   U 3p Si1    ; LINEFIELD(static/effectiveU, 3, 4) ; 0.16123
match ;   U 3p Si2    ; LINEFIELD(static/effectiveU, 4, 4) ; 0.16123

Precision: 5.96e-07
match ;   Occupation Ni2 up-down 3d4   ; LINEFIELD(static/occ_matrices, -2, 3) ; 1.1918265
match ;   Occupation Ni2 up-down 3d5   ; LINEFIELD(static/occ_matrices, -1, 5) ; 1.1918265

Precision: 1.00e-04
match ;    k-point 1 (x)    ; GREPFIELD(static/info, '#k =       1', 7) ; 0.0
match ;    k-point 1 (y)    ; GREPFIELD(static/info, '#k =       1', 8) ; 0.0
match ;    k-point 1 (z)    ; GREPFIELD(static/info, '#k =       1', 9) ; 0.0
Precision: 1.42e-04
match ;    Eigenvalue  1    ; GREPFIELD(static/info, '#k =       1', 3, 1) ; -0.28447
Precision: 7.93e-06
match ;    Eigenvalue  2    ; GREPFIELD(static/info, '#k =       1', 3, 2) ; 0.158536
Precision: 7.93e-06
match ;    Eigenvalue  3    ; GREPFIELD(static/info, '#k =       1', 3, 3) ; 0.158536
Precision: 7.93e-06
match ;    Eigenvalue  4    ; GREPFIELD(static/info, '#k =       1', 3, 4) ; 0.158536

Input      : 08-loewdin.02-intersite.inp

#We have MaximumIter = 1
match ;  SCF convergence  ; GREPCOUNT(static/info, 'SCF converged') ; 0.0

Precision: 3.86e-07
match ;   Total energy         ; GREPFIELD(static/info, 'Total       =', 3) ; -7.7285556600000005
Precision: 3.93e-06
match ;   Ion-ion energy       ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -7.8578008
Precision: 7.21e-07
match ;   Eigenvalues sum      ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -0.1441112
Precision: 2.88e-06
match ;   Hartree energy       ; GREPFIELD(static/info, 'Hartree     =', 3) ; 0.5752933
Precision: 1.02e-07
match ;   Exchange energy      ; GREPFIELD(static/info, 'Exchange    =', 3) ; -2.0444346799999997
Precision: 1.88e-07
match ;   Correlation energy   ; GREPFIELD(static/info, 'Correlation =', 3) ; -0.37575379
Precision: 1.57e-05
match ;   Kinetic energy       ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 3.148749
Precision: 6.58e-08
match ;   External energy      ; GREPFIELD(static/info, 'External    =', 3) ; -1.31665046
Precision: 1.34e-07
match ;   Hubbard energy       ; GREPFIELD(static/info, 'Hubbard     =', 3) ; 0.026793820000000003

Precision: 4.45e-05
match ;   U 3p Si1    ; LINEFIELD(static/effectiveU, 3, 4) ; 0.088939
match ;   U 3p Si2    ; LINEFIELD(static/effectiveU, 4, 4) ; 0.088939
Precision: 2.81e-05
match ;   V 3p-3p   ; GREPFIELD(static/info, 'Effective intersite V', 7, 3) ; 0.056182

#Values are copied from the test above
Input      : 08-loewdin.03-intersite_domains.inp

match ;  SCF convergence  ; GREPCOUNT(static/info, 'SCF converged') ; 0.0

Precision: 3.86e-07
match ;   Total energy         ; GREPFIELD(static/info, 'Total       =', 3) ; -7.7285556600000005
Precision: 3.93e-06
match ;   Ion-ion energy       ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -7.8578008
Precision: 7.21e-07
match ;   Eigenvalues sum      ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -0.1441112
Precision: 2.88e-06
match ;   Hartree energy       ; GREPFIELD(static/info, 'Hartree     =', 3) ; 0.5752933
Precision: 1.02e-07
match ;   Exchange energy      ; GREPFIELD(static/info, 'Exchange    =', 3) ; -2.0444346799999997
Precision: 1.88e-07
match ;   Correlation energy   ; GREPFIELD(static/info, 'Correlation =', 3) ; -0.37575379
Precision: 1.57e-05
match ;   Kinetic energy       ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 3.148749
Precision: 6.58e-08
match ;   External energy      ; GREPFIELD(static/info, 'External    =', 3) ; -1.31665046
Precision: 1.34e-07
match ;   Hubbard energy       ; GREPFIELD(static/info, 'Hubbard     =', 3) ; 0.026793820000000003

Precision: 4.45e-05
match ;   U 3p Si1    ; LINEFIELD(static/effectiveU, 3, 4) ; 0.088939
match ;   U 3p Si2    ; LINEFIELD(static/effectiveU, 4, 4) ; 0.088939
Precision: 2.81e-05
match ;   V 3p-3p   ; GREPFIELD(static/info, 'Effective intersite V', 7, 3) ; 0.056182
