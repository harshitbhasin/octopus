# -*- coding: utf-8 mode: shell-script -*-

Test       : SOC in periodic systems
Program    : octopus
TestGroups : long-run, periodic_systems
Enabled    : Yes

Input      : 29-soc_solids.01-gs.inp

match ; SCF convergence ; GREPCOUNT(static/info, 'SCF converged') ; 1

match ; Total k-points   ; GREPFIELD(static/info, 'Total number of k-points', 6) ; 3
match ; Space group        ; GREPFIELD(out, 'Space group', 4) ; 229
match ; No. of symmetries  ; GREPFIELD(out, 'symmetries that can be used', 5)  ;  48

Precision: 2.68e-07
match ;     Total energy           ; GREPFIELD(static/info, 'Total       =', 3) ; -5.36260827
Precision: 3.28e-07
match ;     Ion-ion energy         ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -6.56757813
Precision: 4.04e-07
match ;     Eigenvalues sum        ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; 0.80839904
Precision: 3.19e-07
match ;     Hartree energy         ; GREPFIELD(static/info, 'Hartree     =', 3) ; 0.06384904000000001
Precision: 6.40e-08
match ;     Exchange energy        ; GREPFIELD(static/info, 'Exchange    =', 3) ; -1.27908033
Precision: 1.18e-07
match ;     Correlation energy     ; GREPFIELD(static/info, 'Correlation =', 3) ; -0.23586828
Precision: 1.15e-07
match ;     Kinetic energy         ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 2.30064707
Precision: 1.78e-07
match ;     External energy        ; GREPFIELD(static/info, 'External    =', 3) ; 0.35542236

Precision: 1.32e-05
match ;    Eigenvalue  1    ; GREPFIELD(static/info, '#k =       1', 3, 1) ; -0.264508
Precision: 1.32e-05
match ;    Eigenvalue  2    ; GREPFIELD(static/info, '#k =       1', 3, 2) ; -0.264508
Precision: 2.92e-05
match ;    Eigenvalue  4    ; GREPFIELD(static/info, '#k =       1', 3, 4) ; 0.584526
Precision: 3.82e-07
match ;    Eigenvalue  5    ; GREPFIELD(static/info, '#k =       1', 3, 5) ; 0.763268

# Testing the commutator [r,V_NL] through the current
Precision: 5.18e-14
match ;       x current sp 1      ; LINEFIELD(static/current-sp1-x.x\=0\,y\=0, 5, 2) ; -0.00146598501932793
Precision: 5.20e-14
match ;       x current sp 2      ; LINEFIELD(static/current-sp2-x.x\=0\,y\=0, 5, 2) ; 0.00146598501932796
Precision: 5.17e-14
match ;       x current sp 3      ; LINEFIELD(static/current-sp3-x.x\=0\,y\=0, 5, 2) ; -0.0038665397750374396
Precision: 2.75e-14
match ;       x current sp 4      ; LINEFIELD(static/current-sp4-x.x\=0\,y\=0, 5, 2) ; 0.0038580952231125


# Testing the forces
# Tolerances are too high for been really meaningful
Precision: 3.46e-12
match ;      z Force Local        ; LINEFIELD(static/forces, 2, 14) ; 3.1576569500000003e-13
Precision: 4.48e-12
match ;      z Force NL           ; LINEFIELD(static/forces, 2, 17) ; 4.2365839e-13

Precision: 5.1e-05
match ;  Total Magnetic Moment - x ; GREPFIELD(static/info, 'mx = ', 3) ; 0.000000
match ;  Total Magnetic Moment - y ; GREPFIELD(static/info, 'my = ', 6) ; 0.000000
match ;  Total Magnetic Moment - z ; GREPFIELD(static/info, 'mz = ', 9) ; 0.000000


