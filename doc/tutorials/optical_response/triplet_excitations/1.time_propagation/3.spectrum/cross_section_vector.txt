# nspin         2
# kick mode    1
# kick strength    0.005291772086
# dim           3
# pol(1)           1.000000000000    0.000000000000    0.000000000000
# pol(2)           0.000000000000    1.000000000000    0.000000000000
# pol(3)           0.000000000000    0.000000000000    1.000000000000
# direction    1
# Equiv. axes  0
# wprime           0.000000000000    0.000000000000    1.000000000000
# kick time        0.000000000000
#%
# Number of time steps =     2500
# PropagationSpectrumDampMode   =    2
# PropagationSpectrumDampFactor =   -27.2114
# PropagationSpectrumStartTime  =     0.0000
# PropagationSpectrumEndTime    =    10.0000
# PropagationSpectrumMinEnergy  =     0.0000
# PropagationSpectrumMaxEnergy  =    20.0000
# PropagationSpectrumEnergyStep =     0.0100
#%
#       Energy        sigma(1, nspin=1)   sigma(2, nspin=1)   sigma(3, nspin=1)   sigma(1, nspin=2)   sigma(2, nspin=2)   sigma(3, nspin=2)   StrengthFunction(1) StrengthFunction(2)
#        [eV]               [A^2]               [A^2]               [A^2]               [A^2]               [A^2]               [A^2]               [1/eV]              [1/eV]       
      0.00000000E+00     -0.00000000E+00     -0.00000000E+00     -0.00000000E+00     -0.00000000E+00     -0.00000000E+00     -0.00000000E+00     -0.00000000E+00     -0.00000000E+00
      0.10000000E-01     -0.41162108E-08     -0.27945647E-16      0.45982882E-16      0.40117139E-08      0.64453830E-16      0.31116227E-16     -0.37501587E-08      0.36549547E-08
      0.20000000E-01     -0.16407193E-07     -0.11165153E-15      0.18372002E-15      0.15989541E-07      0.25750965E-15      0.12430761E-15     -0.14948112E-07      0.14567601E-07
      0.30000000E-01     -0.36700806E-07     -0.25072524E-15      0.41257814E-15      0.35762345E-07      0.57825221E-15      0.27910319E-15     -0.33437026E-07      0.32582022E-07
