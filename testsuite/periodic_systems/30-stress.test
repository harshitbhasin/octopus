# -*- coding: utf-8 mode: shell-script -*-

Test       : Stress tensor
Program    : octopus
TestGroups : short-run, periodic_systems
Enabled    : Yes


Processors : 4
Precision  : 1e-7

# TODO: UPDATE THE REFERENCE VALUES AFTER FIXING THE INDEPENDENT PARTICLE RUN https://gitlab.com/octopus-code/octopus/-/issues/696
# Input      : 30-stress.01-independent.inp

# match ;     Stress (11)      ; GREPFIELD(static/info, 'Total stress tensor', 2, 2) ; -0.005645947312
# match ;     Stress (12)      ; GREPFIELD(static/info, 'Total stress tensor', 3, 2) ; -0.006088082498
# match ;     Stress (13)      ; GREPFIELD(static/info, 'Total stress tensor', 4, 2) ; -0.00586914639
# match ;     Stress (21)      ; GREPFIELD(static/info, 'Total stress tensor', 2, 3) ; -0.00039022221429999996
# match ;     Stress (22)      ; GREPFIELD(static/info, 'Total stress tensor', 3, 3) ; -0.001457407419
# match ;     Stress (23)      ; GREPFIELD(static/info, 'Total stress tensor', 4, 3) ; -0.0005861471761
# match ;     Stress (31)      ; GREPFIELD(static/info, 'Total stress tensor', 2, 4) ; -0.0021573512889999998
# match ;     Stress (32)      ; GREPFIELD(static/info, 'Total stress tensor', 3, 4) ; 0.0007357126078
# match ;     Stress (33)      ; GREPFIELD(static/info, 'Total stress tensor', 4, 4) ; 0.0001390268731

# match ;   Pressure (H/b^3)   ; GREPFIELD(static/info, 'Pressure \[H/b^3\]', 4) ; 0.0058677254
# match ;   Pressure (GPa)     ; GREPFIELD(static/info, 'Pressure \[GPa\]', 8) ; 172.63450439

# match ;  Kinetic stress (11) ; GREPFIELD(static/stress, 'Kinetic stress tensor', 2, 2) ; 1.222914757E-02
# match ;  Kinetic stress (12) ; GREPFIELD(static/stress, 'Kinetic stress tensor', 3, 2) ; 7.178671366E-05
# match ;  Kinetic stress (13) ; GREPFIELD(static/stress, 'Kinetic stress tensor', 4, 2) ; -5.529174315E-05
# match ;  Kinetic stress (21) ; GREPFIELD(static/stress, 'Kinetic stress tensor', 2, 3) ; 7.178671366E-05
# match ;  Kinetic stress (22) ; GREPFIELD(static/stress, 'Kinetic stress tensor', 3, 3) ; 1.228532386E-02
# match ;  Kinetic stress (23) ; GREPFIELD(static/stress, 'Kinetic stress tensor', 4, 3) ; 6.562294732E-05
# match ;  Kinetic stress (31) ; GREPFIELD(static/stress, 'Kinetic stress tensor', 2, 4) ; -5.529174315E-05
# match ;  Kinetic stress (32) ; GREPFIELD(static/stress, 'Kinetic stress tensor', 3, 4) ; 6.562294732E-05
# match ;  Kinetic stress (33) ; GREPFIELD(static/stress, 'Kinetic stress tensor', 4, 4) ; 1.226402391E-02

# match ;  Hartree stress (11) ; GREPFIELD(static/stress, 'Hartree stress tensor', 2, 2) ; 0.000686624679
# match ;  Hartree stress (12) ; GREPFIELD(static/stress, 'Hartree stress tensor', 3, 2) ; 0.000110529178
# match ;  Hartree stress (13) ; GREPFIELD(static/stress, 'Hartree stress tensor', 4, 2) ; -6.586250444e-05
# match ;  Hartree stress (21) ; GREPFIELD(static/stress, 'Hartree stress tensor', 2, 3) ; 0.000110529178
# match ;  Hartree stress (22) ; GREPFIELD(static/stress, 'Hartree stress tensor', 3, 3) ; 0.0008987790436999999
# match ;  Hartree stress (23) ; GREPFIELD(static/stress, 'Hartree stress tensor', 4, 3) ; 0.0001595756753
# match ;  Hartree stress (31) ; GREPFIELD(static/stress, 'Hartree stress tensor', 2, 4) ; -6.586250444e-05
# match ;  Hartree stress (32) ; GREPFIELD(static/stress, 'Hartree stress tensor', 3, 4) ; 0.0001595756753
# match ;  Hartree stress (33) ; GREPFIELD(static/stress, 'Hartree stress tensor', 4, 4) ; 0.000809552841

# match ;  XC stress (11)      ; GREPFIELD(static/stress, 'XC stress tensor', 2, 2) ; -9.675418234E-04
# match ;  XC stress (12)      ; GREPFIELD(static/stress, 'XC stress tensor', 3, 2) ; 0
# match ;  XC stress (13)      ; GREPFIELD(static/stress, 'XC stress tensor', 4, 2) ; 0
# match ;  XC stress (21)      ; GREPFIELD(static/stress, 'XC stress tensor', 2, 3) ; 0
# match ;  XC stress (22)      ; GREPFIELD(static/stress, 'XC stress tensor', 3, 3) ; -9.675418234E-04
# match ;  XC stress (23)      ; GREPFIELD(static/stress, 'XC stress tensor', 4, 3) ; 0
# match ;  XC stress (31)      ; GREPFIELD(static/stress, 'XC stress tensor', 2, 4) ; 0
# match ;  XC stress (32)      ; GREPFIELD(static/stress, 'XC stress tensor', 3, 4) ; 0
# match ;  XC stress (33)      ; GREPFIELD(static/stress, 'XC stress tensor', 4, 4) ; -9.675418234E-04

# match ; Pseudopotential stress (11) ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 2, 2) ; 0.006049504587
# match ; Pseudopotential stress (12) ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 3, 2) ; -0.0009211717667000001
# match ; Pseudopotential stress (13) ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 4, 2) ; 0.00051617285
# match ; Pseudopotential stress (21) ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 2, 3) ; 0.000146013438
# match ; Pseudopotential stress (22) ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 3, 3) ; 0.005097509393
# match ; Pseudopotential stress (23) ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 4, 3) ; -0.0013618088610000002
# match ; Pseudopotential stress (31) ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 2, 4) ; -8.051288476e-05
# match ; Pseudopotential stress (32) ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 3, 4) ; 0.0002093952519
# match ; Pseudopotential stress (33) ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 4, 4) ; 0.005499784882999999

# match ;  Ion-ion stress (11) ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 2, 2) ; -1.235178770E-02
# match ;  Ion-ion stress (12) ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 3, 2) ; 1.129036311E-03
# match ;  Ion-ion stress (13) ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 4, 2) ; -5.340350017E-04
# match ;  Ion-ion stress (21) ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 2, 3) ;  1.129036311E-03
# match ;  Ion-ion stress (22) ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 3, 3) ;  -1.122598798E-02
# match ;  Ion-ion stress (23) ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 4, 3) ;1.722698484E-03
# match ;  Ion-ion stress (31) ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 2, 4) ; -5.340350017E-04
# match ;  Ion-ion stress (32) ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 3, 4) ; 1.722698484E-03
# match ;  Ion-ion stress (33) ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 4, 4) ;  -1.173667342E-02


Input      : 30-stress.02-gamma_point.inp

Precision: 2.62e-11
match ;     Pressure (H/b^3)     ; GREPFIELD(static/info, 'Pressure \[H/b^3\]', 4) ; 0.000524495897
Precision: 7.72e-08
match ;     Pressure (GPa)       ; GREPFIELD(static/info, 'Pressure \[GPa\]', 8) ; 15.431207659999998
Precision: 4.51e-11
match ;    Kinetic stress (11)   ; GREPFIELD(static/stress, 'Kinetic stress tensor', 2, 2) ; 0.009023875525
Precision: 1.00e-07
match ;    Kinetic stress (12)   ; GREPFIELD(static/stress, 'Kinetic stress tensor', 3, 2) ; -3.17329496e-15
Precision: 1.00e-07
match ;    Kinetic stress (13)   ; GREPFIELD(static/stress, 'Kinetic stress tensor', 4, 2) ; 3.918400326e-15
Precision: 1.00e-07
match ;    Kinetic stress (21)   ; GREPFIELD(static/stress, 'Kinetic stress tensor', 2, 3) ; -3.17329496e-15
Precision: 4.51e-11
match ;    Kinetic stress (22)   ; GREPFIELD(static/stress, 'Kinetic stress tensor', 3, 3) ; 0.009023875524
Precision: 1.00e-07
match ;    Kinetic stress (23)   ; GREPFIELD(static/stress, 'Kinetic stress tensor', 4, 3) ; -9.48369161e-15
Precision: 1.00e-07
match ;    Kinetic stress (31)   ; GREPFIELD(static/stress, 'Kinetic stress tensor', 2, 4) ; 3.918400326e-15
Precision: 1.00e-07
match ;    Kinetic stress (32)   ; GREPFIELD(static/stress, 'Kinetic stress tensor', 3, 4) ; -9.48369161e-15
Precision: 4.51e-11
match ;    Kinetic stress (33)   ; GREPFIELD(static/stress, 'Kinetic stress tensor', 4, 4) ; 0.009023875525
Precision: 4.10e-12
match ;    Hartree stress (11)   ; GREPFIELD(static/stress, 'Hartree stress tensor', 2, 2) ; 0.0008204224734
Precision: 1.00e-07
match ;    Hartree stress (12)   ; GREPFIELD(static/stress, 'Hartree stress tensor', 3, 2) ; 5.083111337e-20
Precision: 1.00e-07
match ;    Hartree stress (13)   ; GREPFIELD(static/stress, 'Hartree stress tensor', 4, 2) ; -2.719041979e-21
Precision: 1.00e-07
match ;    Hartree stress (21)   ; GREPFIELD(static/stress, 'Hartree stress tensor', 2, 3) ; 5.083111337e-20
Precision: 4.10e-12
match ;    Hartree stress (22)   ; GREPFIELD(static/stress, 'Hartree stress tensor', 3, 3) ; 0.0008204224734
Precision: 1.00e-07
match ;    Hartree stress (23)   ; GREPFIELD(static/stress, 'Hartree stress tensor', 4, 3) ; -3.976337293e-22
Precision: 1.00e-07
match ;    Hartree stress (31)   ; GREPFIELD(static/stress, 'Hartree stress tensor', 2, 4) ; -2.719041979e-21
Precision: 1.00e-07
match ;    Hartree stress (32)   ; GREPFIELD(static/stress, 'Hartree stress tensor', 3, 4) ; -3.976337293e-22
Precision: 4.10e-12
match ;    Hartree stress (33)   ; GREPFIELD(static/stress, 'Hartree stress tensor', 4, 4) ; 0.0008204224734
Precision: 1.82e-11
match ;    XC stress (11)        ; GREPFIELD(static/stress, 'XC stress tensor', 2, 2) ; -0.003638693759
Precision: 1.00e-07
match ;    XC stress (12)        ; GREPFIELD(static/stress, 'XC stress tensor', 3, 2) ; 0.0
Precision: 1.00e-07
match ;    XC stress (13)        ; GREPFIELD(static/stress, 'XC stress tensor', 4, 2) ; 0.0
Precision: 1.00e-07
match ;    XC stress (21)        ; GREPFIELD(static/stress, 'XC stress tensor', 2, 3) ; 0.0
Precision: 1.82e-11
match ;    XC stress (22)        ; GREPFIELD(static/stress, 'XC stress tensor', 3, 3) ; -0.003638693759
Precision: 1.00e-07
match ;    XC stress (23)        ; GREPFIELD(static/stress, 'XC stress tensor', 4, 3) ; 0.0
Precision: 1.00e-07
match ;    XC stress (31)        ; GREPFIELD(static/stress, 'XC stress tensor', 2, 4) ; 0.0
Precision: 1.00e-07
match ;    XC stress (32)        ; GREPFIELD(static/stress, 'XC stress tensor', 3, 4) ; 0.0
Precision: 1.82e-11
match ;    XC stress (33)        ; GREPFIELD(static/stress, 'XC stress tensor', 4, 4) ; -0.003638693759
Precision: 1.65e-11
match ;   Pseudopotential stress (11)   ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 2, 2) ; 0.0032974533620000004
Precision: 1.00e-07
match ;   Pseudopotential stress (12)   ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 3, 2) ; 3.13574937e-17
Precision: 1.00e-07
match ;   Pseudopotential stress (13)   ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 4, 2) ; -3.010628796e-15
Precision: 1.00e-07
match ;   Pseudopotential stress (21)   ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 2, 3) ; 3.137972609e-17
Precision: 1.65e-11
match ;   Pseudopotential stress (22)   ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 3, 3) ; 0.0032974533620000004
Precision: 1.00e-07
match ;   Pseudopotential stress (23)   ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 4, 3) ; 6.907277312e-15
Precision: 1.00e-07
match ;   Pseudopotential stress (31)   ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 2, 4) ; -3.010580118e-15
Precision: 1.00e-07
match ;   Pseudopotential stress (32)   ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 3, 4) ; 6.907342906e-15
Precision: 1.65e-11
match ;   Pseudopotential stress (33)   ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 4, 4) ; 0.0032974533620000004
Precision: 1.00e-07
match ;    Ion-ion stress (11)   ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 2, 2) ; -0.008978561705
Precision: 1.00e-07
match ;    Ion-ion stress (12)   ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 3, 2) ; -4.451112881e-19
Precision: 1.00e-07
match ;    Ion-ion stress (13)   ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 4, 2) ; 6.623349638e-20
Precision: 1.00e-07
match ;    Ion-ion stress (21)   ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 2, 3) ; -4.451248145e-19
Precision: 1.00e-07
match ;    Ion-ion stress (22)   ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 3, 3) ; -0.008978561705
Precision: 1.00e-07
match ;    Ion-ion stress (23)   ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 4, 3) ; -1.22990697e-19
Precision: 1.00e-07
match ;    Ion-ion stress (31)   ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 2, 4) ; 6.623180208e-20
Precision: 1.00e-07
match ;    Ion-ion stress (32)   ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 3, 4) ; -1.229906706e-19
Precision: 1.00e-07
match ;    Ion-ion stress (33)   ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 4, 4) ; -0.008978561705
Precision: 2.62e-12
match ;       Stress (11)        ; GREPFIELD(static/info, 'Total stress tensor', 2, 2) ; -0.0005244958964999999
Precision: 1.00e-07
match ;       Stress (12)        ; GREPFIELD(static/info, 'Total stress tensor', 3, 2) ; 3.142331747e-15
Precision: 1.00e-07
match ;       Stress (13)        ; GREPFIELD(static/info, 'Total stress tensor', 4, 2) ; -9.078350444e-16
Precision: 1.00e-07
match ;       Stress (21)        ; GREPFIELD(static/info, 'Total stress tensor', 2, 3) ; 3.142331747e-15
Precision: 2.62e-12
match ;       Stress (22)        ; GREPFIELD(static/info, 'Total stress tensor', 3, 3) ; -0.0005244958964000001
Precision: 1.00e-07
match ;       Stress (23)        ; GREPFIELD(static/info, 'Total stress tensor', 4, 3) ; 2.576537686e-15
Precision: 1.00e-07
match ;       Stress (31)        ; GREPFIELD(static/info, 'Total stress tensor', 2, 4) ; -9.078837213e-16
Precision: 1.00e-07
match ;       Stress (32)        ; GREPFIELD(static/info, 'Total stress tensor', 3, 4) ; 2.576472092e-15
Precision: 2.62e-12
match ;       Stress (33)        ; GREPFIELD(static/info, 'Total stress tensor', 4, 4) ; -0.0005244958964000001

Input      : 30-stress.03-par_kpoints.inp

Precision: 5.21e-05
match ;      Pressure (H/b^3)      ; GREPFIELD(static/info, 'Pressure \[H/b^3\]', 4) ; 0.0019382619000000002
Precision: 1.53e+00
match ;      Pressure (GPa)        ; GREPFIELD(static/info, 'Pressure \[GPa\]', 8) ; 57.025655519999994
Precision: 2.49e-05
match ;     Kinetic stress (11)    ; GREPFIELD(static/stress, 'Kinetic stress tensor', 2, 2) ; 0.01222820519
Precision: 8.15e-07
match ;     Kinetic stress (12)    ; GREPFIELD(static/stress, 'Kinetic stress tensor', 3, 2) ; 7.192959795e-05
Precision: 1.65e-08
match ;     Kinetic stress (13)    ; GREPFIELD(static/stress, 'Kinetic stress tensor', 4, 2) ; -5.54198589e-05
Precision: 8.15e-07
match ;     Kinetic stress (21)    ; GREPFIELD(static/stress, 'Kinetic stress tensor', 2, 3) ; 7.192959795e-05
Precision: 2.80e-05
match ;     Kinetic stress (22)    ; GREPFIELD(static/stress, 'Kinetic stress tensor', 3, 3) ; 0.01228419728
Precision: 1.96e-06
match ;     Kinetic stress (23)    ; GREPFIELD(static/stress, 'Kinetic stress tensor', 4, 3) ; 6.57625e-05
Precision: 1.65e-08
match ;     Kinetic stress (31)    ; GREPFIELD(static/stress, 'Kinetic stress tensor', 2, 4) ; -5.54198589e-05
Precision: 1.96e-06
match ;     Kinetic stress (32)    ; GREPFIELD(static/stress, 'Kinetic stress tensor', 3, 4) ; 6.57625e-05
Precision: 2.39e-05
match ;     Kinetic stress (33)    ; GREPFIELD(static/stress, 'Kinetic stress tensor', 4, 4) ; 0.01226282047
Precision: 3.79e-06
match ;     Hartree stress (11)    ; GREPFIELD(static/stress, 'Hartree stress tensor', 2, 2) ; 0.000686624679
Precision: 8.83e-07
match ;     Hartree stress (12)    ; GREPFIELD(static/stress, 'Hartree stress tensor', 3, 2) ; 0.00011065499980000001
Precision: 1.80e-07
match ;     Hartree stress (13)    ; GREPFIELD(static/stress, 'Hartree stress tensor', 4, 2) ; -6.586250444e-05
Precision: 8.83e-07
match ;     Hartree stress (21)    ; GREPFIELD(static/stress, 'Hartree stress tensor', 2, 3) ; 0.00011065499980000001
Precision: 4.26e-06
match ;     Hartree stress (22)    ; GREPFIELD(static/stress, 'Hartree stress tensor', 3, 3) ; 0.0008987790436999999
Precision: 7.28e-07
match ;     Hartree stress (23)    ; GREPFIELD(static/stress, 'Hartree stress tensor', 4, 3) ; 0.00015979097859999998
Precision: 1.80e-07
match ;     Hartree stress (31)    ; GREPFIELD(static/stress, 'Hartree stress tensor', 2, 4) ; -6.586250444e-05
Precision: 7.28e-07
match ;     Hartree stress (32)    ; GREPFIELD(static/stress, 'Hartree stress tensor', 3, 4) ; 0.00015979097859999998
Precision: 3.79e-06
match ;     Hartree stress (33)    ; GREPFIELD(static/stress, 'Hartree stress tensor', 4, 4) ; 0.0008095528410000001
Precision: 3.17e-06
match ;     XC stress (11)         ; GREPFIELD(static/stress, 'XC stress tensor', 2, 2) ; -0.004897348708
Precision: 1.60e-07
match ;     XC stress (12)         ; GREPFIELD(static/stress, 'XC stress tensor', 3, 2) ; -1.5604235140000002e-05
Precision: 4.39e-08
match ;     XC stress (13)         ; GREPFIELD(static/stress, 'XC stress tensor', 4, 2) ; 9.043122461e-06
Precision: 1.60e-07
match ;     XC stress (21)         ; GREPFIELD(static/stress, 'XC stress tensor', 2, 3) ; -1.5604235140000002e-05
Precision: 3.18e-06
match ;     XC stress (22)         ; GREPFIELD(static/stress, 'XC stress tensor', 3, 3) ; -0.004908917455
Precision: 1.85e-07
match ;     XC stress (23)         ; GREPFIELD(static/stress, 'XC stress tensor', 4, 3) ; -2.23075642e-05
Precision: 4.39e-08
match ;     XC stress (31)         ; GREPFIELD(static/stress, 'XC stress tensor', 2, 4) ; 9.043122461e-06
Precision: 1.85e-07
match ;     XC stress (32)         ; GREPFIELD(static/stress, 'XC stress tensor', 3, 4) ; -2.23075642e-05
Precision: 3.06e-06
match ;     XC stress (33)         ; GREPFIELD(static/stress, 'XC stress tensor', 4, 4) ; -0.004903588391
Precision: 2.73e-05
match ;    Pseudopotential stress (11)    ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 2, 2) ; 0.006056950999
Precision: 1.14e-06
match ;    Pseudopotential stress (12)    ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 3, 2) ; -0.0009211717667000001
Precision: 5.83e-07
match ;    Pseudopotential stress (13)    ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 4, 2) ; 0.0005164792825
Precision: 1.12e-06
match ;    Pseudopotential stress (21)    ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 2, 3) ; -0.0009211925238
Precision: 2.92e-05
match ;    Pseudopotential stress (22)    ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 3, 3) ; 0.00510484188
Precision: 1.46e-06
match ;    Pseudopotential stress (23)    ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 4, 3) ; -0.0013619174580000002
Precision: 5.83e-07
match ;    Pseudopotential stress (31)    ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 2, 4) ; 0.0005164792825
Precision: 1.46e-06
match ;    Pseudopotential stress (32)    ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 3, 4) ; -0.0013619174580000002
Precision: 2.55e-05
match ;    Pseudopotential stress (33)    ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 4, 4) ; 0.005507159336000001
Precision: 1.00e-07
match ;     Ion-ion stress (11)    ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 2, 2) ; -0.0123517877
Precision: 1.00e-07
match ;     Ion-ion stress (12)    ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 3, 2) ; 0.001129036311
Precision: 1.00e-07
match ;     Ion-ion stress (13)    ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 4, 2) ; -0.0005340350017
Precision: 1.00e-07
match ;     Ion-ion stress (21)    ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 2, 3) ; 0.001129036311
Precision: 1.00e-07
match ;     Ion-ion stress (22)    ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 3, 3) ; -0.01122598798
Precision: 1.00e-07
match ;     Ion-ion stress (23)    ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 4, 3) ; 0.001722698484
Precision: 1.00e-07
match ;     Ion-ion stress (31)    ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 2, 4) ; -0.0005340350017
Precision: 1.00e-07
match ;     Ion-ion stress (32)    ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 3, 4) ; 0.001722698484
Precision: 1.00e-07
match ;     Ion-ion stress (33)    ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 4, 4) ; -0.01173667342
Precision: 5.15e-05
match ;        Stress (11)         ; GREPFIELD(static/info, 'Total stress tensor', 2, 2) ; -0.001722587536
Precision: 8.91e-07
match ;        Stress (12)         ; GREPFIELD(static/info, 'Total stress tensor', 3, 2) ; -0.0003748241502
Precision: 4.39e-07
match ;        Stress (13)         ; GREPFIELD(static/info, 'Total stress tensor', 4, 2) ; 0.00012985280590000002
Precision: 8.91e-07
match ;        Stress (21)         ; GREPFIELD(static/info, 'Total stress tensor', 2, 3) ; -0.0003748241502
Precision: 5.62e-05
match ;        Stress (22)         ; GREPFIELD(static/info, 'Total stress tensor', 3, 3) ; -0.002152924877
Precision: 2.51e-06
match ;        Stress (23)         ; GREPFIELD(static/info, 'Total stress tensor', 4, 3) ; -0.0005640269404
Precision: 4.39e-07
match ;        Stress (31)         ; GREPFIELD(static/info, 'Total stress tensor', 2, 4) ; 0.00012985280590000002
Precision: 2.51e-06
match ;        Stress (32)         ; GREPFIELD(static/info, 'Total stress tensor', 3, 4) ; -0.0005640269404
Precision: 4.88e-05
match ;        Stress (33)         ; GREPFIELD(static/info, 'Total stress tensor', 4, 4) ; -0.001939273371

Input      : 30-stress.04-kpoint_sym.inp

Precision: 9.04e-12
match ;         Stress (11)          ; GREPFIELD(static/info, 'Total stress tensor', 2, 2) ; -0.001807587724
Precision: 1.12e-11
match ;         Stress (22)          ; GREPFIELD(static/info, 'Total stress tensor', 3, 3) ; -0.002243469161
Precision: 1.01e-10
match ;         Stress (33)          ; GREPFIELD(static/info, 'Total stress tensor', 4, 4) ; -0.00201839609
Precision: 1.84e-12
match ;         Stress (12)          ; GREPFIELD(static/info, 'Total stress tensor', 3, 2) ; -0.0003683310069
Precision: 1.84e-12
match ;         Stress (21)          ; GREPFIELD(static/info, 'Total stress tensor', 2, 3) ; -0.0003683310069
Precision: 2.82e-12
match ;         Stress (23)          ; GREPFIELD(static/info, 'Total stress tensor', 4, 3) ; -0.0005637770596000001
Precision: 2.82e-12
match ;         Stress (32)          ; GREPFIELD(static/info, 'Total stress tensor', 3, 4) ; -0.0005637770596000001
Precision: 6.02e-13
match ;         Stress (31)          ; GREPFIELD(static/info, 'Total stress tensor', 2, 4) ; 0.0001203182398
Precision: 6.02e-13
match ;         Stress (13)          ; GREPFIELD(static/info, 'Total stress tensor', 4, 2) ; 0.0001203182398
Precision: 1.01e-10
match ;       Pressure (H/b^3)       ; GREPFIELD(static/info, 'Pressure \[H/b^3\]', 4) ; 0.00202315099
Precision: 2.98e-07
match ;       Pressure (GPa)         ; GREPFIELD(static/info, 'Pressure \[GPa\]', 8) ; 59.5231789

Input      : 30-stress.05-output_scf.inp

Precision: 2.62e-11
match ;        Pressure (H/b^3)        ; GREPFIELD(static/info, 'Pressure \[H/b^3\]', 4) ; 0.000524495897
Precision: 7.72e-08
match ;        Pressure (GPa)          ; GREPFIELD(static/info, 'Pressure \[GPa\]', 8) ; 15.431207659999998
Precision: 2.62e-12
match ;             Stress (xx)              ; GREPFIELD(static/info, 'T_{ij}', 2, 1) ; -0.0005244958964999999
match ;             Stress (yy)              ; GREPFIELD(static/info, 'T_{ij}', 3, 2) ; -0.0005244958964000001
match ;             Stress (zz)              ; GREPFIELD(static/info, 'T_{ij}', 4, 3) ; -0.0005244958964000001
Precision: 5.43e-14
match ;             Stress (xy)              ; GREPFIELD(static/info, 'T_{ij}', 3, 1) ; -2.888292425e-15
match ;             Stress (yx)              ; GREPFIELD(static/info, 'T_{ij}', 2, 2) ; -2.888292415e-15
Precision: 4.13e-14
match ;             Stress (yz)              ; GREPFIELD(static/info, 'T_{ij}', 4, 2) ; -1.08607907e-14
match ;             Stress (zy)              ; GREPFIELD(static/info, 'T_{ij}', 3, 3) ; -1.08607907e-14
Precision: 4.16e-14
match ;             Stress (zx)              ; GREPFIELD(static/info, 'T_{ij}', 2, 3) ; -2.90783600000002e-16
match ;             Stress (xz)              ; GREPFIELD(static/info, 'T_{ij}', 4, 1) ; -2.90783600000002e-16