# -*- coding: utf-8 mode: shell-script -*-

Test       : Real space symmetrization wih LDA
Program    : octopus
TestGroups : long-run, periodic_systems, symmetries
Enabled    : Yes

#Here the reference values should be copied from the calculation without symmetries
Input : 07-symmetrization_lda.01-spg2_sym.inp

match ; Space group        ; GREPFIELD(out, 'Space group', 4) ; 2
match ; No. of symmetries  ; GREPFIELD(out, 'symmetries that can be used', 5)  ; 2
match ; S1 ; GREP(static/info, "   1   0   0     0   1   0     0   0   1", 60 ); 0.000000
match ; S2 ; GREP(static/info, "  -1   0   0     0  -1   0     0   0  -1", 60 ); 0.000000

Precision: 3.90e-07
match ;   Total energy         ; GREPFIELD(static/info, 'Total       =', 3) ; -7.80838257
Precision: 3.27e-07
match ;   Ion-ion energy       ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -6.545369559999999
Precision: 5.04e-07
match ;   Eigenvalues sum      ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -1.0079808
Precision: 4.53e-07
match ;   Hartree energy       ; GREPFIELD(static/info, 'Hartree     =', 3) ; 0.90553129
Precision: 8.96e-08
match ;   Exchange energy      ; GREPFIELD(static/info, 'Exchange    =', 3) ; -1.7924073800000002
Precision: 1.77e-07
match ;   Correlation energy   ; GREPFIELD(static/info, 'Correlation =', 3) ; -0.35324391
Precision: 1.29e-07
match ;   Kinetic energy       ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 2.57186311
Precision: 1.30e-07
match ;   External energy      ; GREPFIELD(static/info, 'External    =', 3) ; -2.59475633

Precision: 4.41e-02
match ;   Force 1 (x)     ; GREPFIELD(static/info, '1        Si', 3) ; 0.031165926370000003
Precision: 6.46e-02
match ;   Force 1 (y)     ; GREPFIELD(static/info, '1        Si', 4) ; 0.0530465471
Precision: 6.35e-02
match ;   Force 1 (z)     ; GREPFIELD(static/info, '1        Si', 5) ; -0.0652195403
Precision: 4.41e-02
match ;   Force 2 (x)     ; GREPFIELD(static/info, '2        Si', 3) ; -0.031165926370000003
Precision: 6.46e-02
match ;   Force 2 (y)     ; GREPFIELD(static/info, '2        Si', 4) ; -0.0530465471
Precision: 6.35e-02
match ;   Force 2 (z)     ; GREPFIELD(static/info, '2        Si', 5) ; 0.0652195403
Precision: 1.00e-01
match ;   Partial charge  1   ; GREPFIELD(static/info, 'Partial ionic charges', 3, 2) ; 4.0

#The following values should be the same by symmetries
Precision: 3.85e-12
match ;   Density value 1     ; LINEFIELD(static/density.y=0\,z=0, 2, 2) ; 0.0366442169759707
match ;   Density value 2     ; LINEFIELD(static/density.y=0\,z=0, 14, 2) ; 0.0366442169759707
Precision: 8.58e-12
match ;   Bader value 1       ; LINEFIELD(static/bader.y=0\,z=0, 2, 2) ; -0.0046492484249464365
match ;   Bader value 2       ; LINEFIELD(static/bader.y=0\,z=0, 14, 2) ; -0.004649248424953

Precision: 1.63e-05
match ;   Eigenvalue [  k=1, n=1   ]   ; GREPFIELD(static/info, '#k =       1', 3, 1) ; -0.325984
Precision: 4.31e-05
match ;   Eigenvalue [  k=1, n=2   ]   ; GREPFIELD(static/info, '#k =       1', 3, 2) ; -0.086256
Precision: 6.69e-06
match ;   Eigenvalue [  k=1, n=3   ]   ; GREPFIELD(static/info, '#k =       1', 3, 3) ; -0.013382
Precision: 1.53e-06
match ;   Eigenvalue [  k=1, n=4   ]   ; GREPFIELD(static/info, '#k =       1', 3, 4) ; 0.030521

#Here the reference values should be copied from the calculation without symmetries
Input : 07-symmetrization_lda.02-spg16_sym.inp

Precision: 1.00e-07
match ;   Total energy         ; GREPFIELD(static/info, 'Total       =', 3) ; -1.99940865
Precision: 2.98e-07
match ;   Ion-ion energy       ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -0.59674741
Precision: 5.41e-08
match ;   Eigenvalues sum      ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -1.08247675
Precision: 3.17e-07
match ;   Hartree energy       ; GREPFIELD(static/info, 'Hartree     =', 3) ; 0.63413334
Precision: 4.32e-07
match ;   Exchange energy      ; GREPFIELD(static/info, 'Exchange    =', 3) ; -0.86397009
Precision: 8.55e-08
match ;   Correlation energy   ; GREPFIELD(static/info, 'Correlation =', 3) ; -0.17102832
Precision: 7.71e-08
match ;   Kinetic energy       ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 1.54157936
Precision: 1.27e-07
match ;   External energy      ; GREPFIELD(static/info, 'External    =', 3) ; -2.54337552
Precision: 3.56e-10
match ;   Force 1 (x)     ; GREPFIELD(static/info, '1         H', 3) ; -0.00712301948
Precision: 3.23e-10
match ;   Force 1 (y)     ; GREPFIELD(static/info, '1         H', 4) ; 0.00645849995
Precision: 1.83e-09
match ;   Force 1 (z)     ; GREPFIELD(static/info, '1         H', 5) ; -0.03664658170000001
Precision: 3.56e-10
match ;   Force 2 (x)     ; GREPFIELD(static/info, '2         H', 3) ; 0.00712301948
Precision: 3.23e-10
match ;   Force 2 (y)     ; GREPFIELD(static/info, '2         H', 4) ; -0.00645849995
Precision: 1.83e-09
match ;   Force 2 (z)     ; GREPFIELD(static/info, '2         H', 5) ; -0.03664658170000001
Precision: 1.00e-01
match ;   Partial charge  1   ; GREPFIELD(static/info, 'Partial ionic charges', 3, 2) ; 1.0
Precision: 1.00e-01
match ;   Partial charge  2   ; GREPFIELD(static/info, 'Partial ionic charges', 3, 3) ; 1.0
Precision: 1.15e-15
match ;   Density value 1     ; LINEFIELD(static/density.y=0\,z=0, 5, 2) ; 0.0230857331245095
Precision: 1.64e-15
match ;   Density value 2     ; LINEFIELD(static/density.y=0\,z=0, 6, 2) ; 0.03275049480466621
Precision: 9.57e-16
match ;   Bader value 1       ; LINEFIELD(static/bader.y=0\,z=0, 5, 2) ; 0.019140347310220403
Precision: 7.02e-15
match ;   Bader value 2       ; LINEFIELD(static/bader.y=0\,z=0, 6, 2) ; 0.014031737583439001
Precision: 1.97e-05
match ;   Eigenvalue [  k=1, n=1   ]   ; GREPFIELD(static/info, '#k =       1', 3, 1) ; -0.394726
Precision: 6.51e-06
match ;   Eigenvalue [  k=1, n=2   ]   ; GREPFIELD(static/info, '#k =       1', 3, 2) ; -0.130186
Precision: 5.71e-06
match ;   Eigenvalue [  k=1, n=3   ]   ; GREPFIELD(static/info, '#k =       1', 3, 3) ; -0.114273

#Here the reference values should be copied from the calculation without symmetries
Input : 07-symmetrization_lda.03-spg75_sym.inp

Precision: 1.05e-07
match ;     Total energy           ; GREPFIELD(static/info, 'Total       =', 3) ; -2.10310977
Precision: 7.43e-08
match ;     Ion-ion energy         ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -0.14850957
Precision: 6.52e-08
match ;     Eigenvalues sum        ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -1.3037902300000002
Precision: 5.02e-08
match ;     Hartree energy         ; GREPFIELD(static/info, 'Hartree     =', 3) ; 1.00344722
Precision: 4.89e-07
match ;     Exchange energy        ; GREPFIELD(static/info, 'Exchange    =', 3) ; -0.97844407
Precision: 9.04e-08
match ;     Correlation energy     ; GREPFIELD(static/info, 'Correlation =', 3) ; -0.18081696
Precision: 8.83e-08
match ;     Kinetic energy         ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 1.7657258100000002
Precision: 1.78e-07
match ;     External energy        ; GREPFIELD(static/info, 'External    =', 3) ; -3.56451223
Precision: 1.94e-10
match ;      Force 1 (x)        ; GREPFIELD(static/info, '1         H', 3) ; 0.00387634161
Precision: 1.69e-10
match ;      Force 1 (y)        ; GREPFIELD(static/info, '1         H', 4) ; 0.00338858151
Precision: 4.60e-17
match ;      Force 1 (z)        ; GREPFIELD(static/info, '1         H', 5) ; 5.7383904600000005e-15
Precision: 1.69e-10
match ;      Force 2 (x)        ; GREPFIELD(static/info, '2         H', 3) ; -0.00338858151
Precision: 1.94e-10
match ;      Force 2 (y)        ; GREPFIELD(static/info, '2         H', 4) ; 0.00387634161
Precision: 3.04e-15
match ;      Force 2 (z)        ; GREPFIELD(static/info, '2         H', 5) ; 6.720054075e-16
Precision: 2.41e-05
match ;     Eigenvalue [  k=1, n=1   ]     ; GREPFIELD(static/info, '#k =       1', 3, 1) ; -0.482946
Precision: 5.73e-06
match ;     Eigenvalue [  k=1, n=2   ]     ; GREPFIELD(static/info, '#k =       1', 3, 2) ; -0.114576
Precision: 5.73e-06
match ;     Eigenvalue [  k=1, n=3   ]     ; GREPFIELD(static/info, '#k =       1', 3, 3) ; -0.114576
Precision: 1.00e-01
match ;      Partial charge  1      ; GREPFIELD(static/info, 'Partial ionic charges', 3, 2) ; 1.0
Precision: 1.00e-01
match ;      Partial charge  2      ; GREPFIELD(static/info, 'Partial ionic charges', 3, 3) ; 1.0
Precision: 3.92e-14
match ;      Density value 1        ; LINEFIELD(static/density.y=0\,z=0, 5, 2) ; 0.0213026347746762
Precision: 1.16e-13
match ;      Density value 2        ; LINEFIELD(static/density.y=0\,z=0, 6, 2) ; 0.031427320862495604
Precision: 2.21e-12
match ;      Bader value 1          ; LINEFIELD(static/bader.y=0\,z=0, 6, 2) ; 0.04403399221249041
Precision: 7.62e-13
match ;      Bader value 2          ; LINEFIELD(static/bader.y=0\,z=0, 9, 2) ; 0.0747347438323272
